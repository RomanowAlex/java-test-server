#### Настройка БД
```
$ psql -h localhost -U postgres
> CREATE database server;
> CREATE role program WITH password 'test';
> GRANT ON PRIVILEGES ON database server TO program;
> ALTER role program WITH login;
> /q
$ psql -h localhost -U program server
```

#### Сброка проекта
Сборка выполняется в jar файл с embedded tomcat-контейнером.

`gradle clean build`

#### Запуск проекта

`gradle clean bootRun`

Запуск происходит как standalone приложение.
После запуска можно зайти в браузере `http://localhost:8080/management/health` - health-чек.  
