package ru.romanow.server.model;

import lombok.Data;

public class PingResponse {
    private String response;

    public PingResponse() {}

    public PingResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
