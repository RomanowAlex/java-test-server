package ru.romanow.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.romanow.server.entity.Person;

public interface PersonRepository
        extends JpaRepository<Person, Integer> { }