package ru.romanow.server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.romanow.server.model.PersonInfo;
import ru.romanow.server.model.PingResponse;
import ru.romanow.server.service.PersonService;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class PersonsController {

    @Autowired
    private PersonService personService;

    @GetMapping("/ping")
    public PingResponse ping() {
        return new PingResponse("ok");
    }

    @GetMapping("/{id}")
    public PersonInfo personById(@PathVariable Integer id) {
        return personService.findPersonById(id);
    }

    @GetMapping
    public List<PersonInfo> findAllPersons() {
        return personService.findAllPersons();
    }
}
