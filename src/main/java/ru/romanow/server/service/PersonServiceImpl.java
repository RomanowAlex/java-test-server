package ru.romanow.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.server.entity.Person;
import ru.romanow.server.model.PersonInfo;
import ru.romanow.server.repository.PersonRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl
        implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public List<PersonInfo> findAllPersons() {
        return personRepository.findAll()
                               .stream()
                               .map(this::buildPersonInfo)
                               .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public PersonInfo findPersonById(@Nonnull Integer id) {
        return personRepository.findById(id).map(this::buildPersonInfo).orElse(null);
    }

    @Nonnull
    private PersonInfo buildPersonInfo(Person person) {
        PersonInfo info = new PersonInfo();
        info.setAge(person.getAge());
        info.setName(person.getName());
        info.setUid(person.getUid());
        return info;
    }
}
