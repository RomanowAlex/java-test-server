package ru.romanow.server.service;

import ru.romanow.server.model.PersonInfo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface PersonService {
    @Nonnull
    List<PersonInfo> findAllPersons();

    @Nullable
    PersonInfo findPersonById(@Nonnull Integer id);
}
